# update-debian
A script to help selectively update Debian (testing).

## Summary
The volume of updates in Debian testing can be impressive. This script helps you to keep the volume of updates down by only preselecting the upstream updates and then letting you refine and execute the update in `aptitude`.

## Description
### Setup
As this script acts as a wrapper or pre-processor for `aptitude` it depends on its configuration for handling Debian in the manner you like to have your Debian installation setup.

The script is to be run as a normal user, which has to have administrative privileges through the sudo system to be able to update packages.

### Dependencies
The following packages have to be available on the system for this script to work:

* perl
* sudo
* aptitude
* apt-show-versions
* apt-mark

### How to use this script
Run the script from the command line as a normal user. If needed, sudo will prompt for your password. It will show you what's going on when updating the package list and preselecting the relevant packages. Once it's done press `Enter` to continue to launch `aptitude`, which will have the relevant packages preselected for update. You can now review and make changes in the proposed updates as you see fit and execute the update when ready.

### How to configure this script
It may be that you want to have certain packages updated always, while others should never be updated. The script offers these options in the top of the script. Open the script with a raw text editor and set `@include` and `@exclude` arrays with regular expressions matching the relevant package names.

The script comes preconfigured to always update the `debian-keyring` and `libc6` packages and skip updates of the `texlive` packages, but these are just examples.

### How this script works
This script takes no parameters, it simply feeds of the information collected from the configured repositories. It uses the Debian package numbering scheme to filter out all `packaging only` updates, leaving the upstream updates for update. Unless on hold, these are then scheduled through `aptitude`, which handles the dependencies.

## License
The MIT license is applicable to this script, as included in the repository.

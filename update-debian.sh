#!/usr/bin/perl
#
# Find the packages which are really new and select them for upgrade.
# This removes respins, changes in packaging, etc.
#
# MIT License
# Copyright 2016 Jaap Keuter
#

# Be verbose or not
$verbose = 0;
$option = shift;
if ($option) {
    $option =~ /-(v{1,2})$/;
    $verbose = length($1 // "");
}

# Package inclusion list
# A regexp list of packages to include in update
@include = ('debian-keyring', 'libc6.*', 'libc-bin');

# Package exclusion list
# A regexp list of packages to exclude from update
@exclude = ('texlive.*');

# No more settings below this line

# Update the package list
@args = ('sudo', 'aptitude', 'update');
system(@args) == 0 or die "system @args failed: $?";

# Get list of packages of interest
$file='/tmp/Debian-versions';
@args = ("apt-show-versions -u > $file");
system(@args) == 0 or die "system @args failed: $?";

open(INFO, $file) or die("Could not open $file.");

foreach $line (<INFO>)  {
    $line =~ '^(.+):.+ (.+) upgradeable to (.+)';
    $package=$1;
    $old_version=$2;
    $new_version=$3;
    undef $old_beta;
    undef $new_beta;

    if ($old_version =~ '^(.+?)((-|~|\+).+)') {
        $old=$1;
        $old_tail=$2;
        if ($old_tail =~ '.+?((\+b|rc)\d+)') {
            $old_beta=$1;
        }
    } else {
        $old=$old_version;
    }

    if ($new_version =~ '^(.+?)((-|~|\+).+)') {
        $new=$1;
        $new_tail=$2;
        if ($new_tail =~ '.+?((\+b|rc)\d+)') {
            $new_beta=$1;
        }
    } else {
        $new=$new_version;
    }

    $upgrade="$package: $old_version ($old$old_beta) -> $new_version ($new$new_beta)\n";
    $verbose==2 && print "$upgrade";
    $verbose==2 && undef $upgrade;

    # Assume we don't upgrade
    $skip_it = 1;

    # Check the inclusion list
    foreach my $include (@include) {
        if ($package =~ $include) {
            $skip_it = 0;
            $verbose && print "$upgrade Forcing upgrade due to inclusion ($include)\n";
            $verbose && undef $upgrade;
            last;
        }
    }

    # Check the version difference or beta promotion
    if (($skip_it == 1) and (($old ne $new)) or (defined $old_beta and not defined $new_beta) ) {
        $skip_it = 0;
        $verbose && print "$upgrade Considering upgrade\n";
        $verbose && undef $upgrade;
        # Check the exclusion list
        foreach my $exclude (@exclude) {
            if ($package =~ $exclude) {
                $skip_it = 1;
                $verbose && print "$upgrade Skipping upgrade due to exclusion ($exclude)\n";
                $verbose && undef $upgrade;
                last;
            }
        }
    }

    # Check if the package is on hold
    if ($skip_it == 0) {
        $hold = `apt-mark showhold $package`;
        chomp $hold;
        if ($package eq $hold) {
            $skip_it = 1;
            $verbose && print "$upgrade Skipping upgrade due to package being held\n";
            $verbose && undef $upgrade;
        }
    }

    # Save the vedict
    if ($skip_it == 0)
    {
        print "Upgrade $package from $old to $new\n";
        push(@packages, $package);
    }
}
close(INFO);

if (@packages) {
    @args = ('sudo', 'aptitude', 'install', '--schedule-only', @packages);
    system(@args) == 0 or die "system @args failed: $?";
} else {
    print "\nNo packages scheduled for upgrade\n";
}

print "\nPress Enter to continue\n";
$command = <STDIN>;

@args = ('sudo', 'aptitude');
system(@args) == 0 or die "system @args failed: $?";
